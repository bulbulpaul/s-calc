package my.culc;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

import my.culc.R;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android_s_calc.Calc;
import android_s_calc.Number;
import android_s_calc.Operation;


public class CulcActivity extends SherlockActivity {

	Calc calc = new Calc();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setTheme(R.style.Theme_Sherlock_Light_DarkActionBar);
		setContentView(R.layout.main);
		
		
		TextView txtDisp = (TextView) findViewById(R.id.display);
		calc.setDisp(txtDisp,this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu){
		menu.add("Setting")
        .setIcon(R.drawable.setting)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent( this, Setting.class );
        startActivity( intent );
	    return true;
	}
	

	public void onClickButton(View view) {
		
		
		//セッティング反映
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean checkbox = pref.getBoolean("checkbox_preference", true);
		if(checkbox == true){
		Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);  
		vibrator.vibrate(33);
		}
		
		
		switch (view.getId()) {
		case R.id.zero:
			calc.onButtonNumber(Number.ZERO);
			break;
			
		case R.id.doublezero:
			calc.onButtonNumber(Number.DOUBLE_ZERO);
			break;
			
		case R.id.one:
			calc.onButtonNumber(Number.ONE);
			break;
			
		case R.id.two:
			calc.onButtonNumber(Number.TWO);
			break;
			
		case R.id.three:
			calc.onButtonNumber(Number.THREE);
			break;
			
		case R.id.four:
			calc.onButtonNumber(Number.FOUR);
			break;
			
		case R.id.five:
			calc.onButtonNumber(Number.FIVE);
			break;
			
		case R.id.six:
			calc.onButtonNumber(Number.SIX);
			break;
			
		case R.id.seven:
			calc.onButtonNumber(Number.SEVEN);
			break;
			
		case R.id.eight:
			calc.onButtonNumber(Number.EIGHT);
			break;
			
		case R.id.nine:
			calc.onButtonNumber(Number.NINE);
			break;

		case R.id.plus:
			calc.onButtonOp(Operation.PLUS);
			break;
			
		case R.id.minus:
			calc.onButtonOp(Operation.MINUS);
			break;
			
		case R.id.times:
			calc.onButtonOp(Operation.TIMES);
			break;
			
		case R.id.divide:
			calc.onButtonOp(Operation.DIVIDE);
			break;

		case R.id.comma:
			calc.onButtonNumber(Number.COMMA);
			break;
			
		case R.id.allclear:
			calc.onButtonAllClear();
			break;

		case R.id.clear:
			calc.onButtonClear();
			break;

		case R.id.equal:
			calc.onButtonEquale();
			break;

		case R.id.sign:
			calc.changeSign();
			break;
			
		case R.id.m_clear:
			calc.onMemoryClear();
			break;
			
		case R.id.m_load:
			calc.onMemoryLoad();
			break;
			
		case R.id.m_plus:
			calc.onMemoryPlus();
			break;
			
		case R.id.m_minus:
			calc.onMemoryMinus();
			break;
			
		case R.id.persent:
			calc.onParsent();
			break;
			
		default:
			break;
		}
	}
}