package android_s_calc;

public class OperationState implements State {

	private static OperationState singleton = new OperationState();

	private OperationState() { 
	}

	public static State getInstance() {
		return singleton;
	}

	//数値ボタン処理
	public void onInputNumber(Context context, Number num) {
		context.clearDisplay();
		context.addDisplayNumber(num);
		if(context.getMH() == false){
			context.clearMH();
		}
		context.changeState(NumberBState.getInstance());
	}

	//演算子ボタン処理
	public void onInputOperation(Context context, Operation op) {
		context.setOp(op);
	}

	//イコールボタン処理
	public void onInputEquale(Context context) {
		switch (context.getOp()) {
		case DIVIDE:
		case TIMES:
			try{
				context.copyAtoB();
				context.doOperation();
				context.changeState(ResultState.getInstance());
			} catch (CalcException e) {
				context.setError();
				context.clearA();
				context.clearB();
				context.clearDisplay();
				context.clearError();
				context.changeState(NumberAState.getInstance());
			}
			break;
		case MINUS:
		case PLUS:
			context.showDisplay(context.getA());
			context.changeState(ResultState.getInstance());
			break;
		default:
			break;
		}
	}

	//クリアボタン処理
	public void onInputClear(Context context) {
		context.clearA();
		context.clearDisplay();
		context.changeState(NumberAState.getInstance());
	}

	//オールクリア処理
	public void onInputAllClear(Context context) {
		context.clearA();
		context.clearB();
		context.clearDisplay();
		context.changeState(NumberAState.getInstance());
	}

	//メモリクリア処理
	public void onInputMemoryClear(Context context) {
		context.clearM();
		
	}

	//メモリロード処理
	public void onInputMemoryLoad(Context context) {
		// TODO Auto-generated method stub
		context.clearDisplay();
		context.showDisplay(context.getM());
		context.setMH(false);
		context.changeState(NumberBState.getInstance());

		
	}
	
	//メモリプラス処理
	public void onInputMemoryPlus(Context context) {
		context.saveDisplayNumberToM_plus();
	}

	//メモリマイナス処理
	public void onInputMemoryMinus(Context context){
		context.saveDisplayNumberToM_minus();
	}
	
	//パーセントボタン処理
	public void onInputParsent(Context context) {
		context.clearA();
		context.clearB();
		context.clearDisplay();
		context.changeState(NumberAState.getInstance());
	}
}
