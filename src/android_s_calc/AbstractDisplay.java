package android_s_calc;

import java.util.Stack;

public abstract class AbstractDisplay {
	//桁数
	protected final int DIGIT = 12;
	//スタックで保持
	protected final Stack<String> displayChar = new Stack<String>();
	//
	protected boolean commaMode;
	
	protected int decimalPlaces;
	//ー
	protected boolean minus;
	
	public abstract void showDisplay(boolean format);
	
	public abstract void onInputNumber(Number num);
	
	public abstract void clear();
	
	public abstract double getNumber();
	
	public abstract void setNumber(double d);
	
	public abstract void setError();
	
	public abstract void clearError();

	public boolean isOverflow(double d) {
		double max = 999999999999d;
		if (d > max) {
			return true;
		}
		return false;
	}

	public String toString() {
		return displayChar.toString();
	}
}
