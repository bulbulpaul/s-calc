package android_s_calc;

public interface Context {
		//状態遷移
		public abstract void changeState(State state);

		//エラー検知
		public abstract double doOperation() throws CalcException;

		//エラー検知（パーセント計算）
		public abstract double doParsentOperation() throws CalcException;

		//ディスプレイ表示
		void showDisplay();

		//ディスプレイ表示
		public abstract void showDisplay(double d);

		//以下各状態でのボタン処理
		public abstract void addDisplayNumber(Number num);
		
		public abstract void saveDisplayNumberToA();

		public abstract void saveDisplayNumberToB();
		
		public abstract void saveDisplayNumberToM_minus();
		
		public abstract void saveDisplayNumberToM_plus();

		public abstract void clearA();

		public abstract void clearB();
		
		public abstract void clearM();
		
		public abstract void clearMH();

		public abstract Operation getOp();

		public abstract void setOp(Operation op);

		public abstract void clearDisplay();

		public abstract void copyAtoB();

		public abstract double getA();

		public abstract void setError();

		public abstract void clearError();

		public abstract void changeSign();

		public abstract double getM();
		
		public abstract boolean getMH();
		
		//メモリ状態
		public abstract void setMH(boolean boo);
}
