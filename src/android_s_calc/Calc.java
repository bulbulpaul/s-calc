package android_s_calc;

import android.widget.TextView;
import android.widget.Toast;

public class Calc implements Context {

	private double A;

	private double B;
	
	//メモリ
	private double M;
	
	//メモリの状態
	private boolean MH;

	//演算子
	private Operation op; 
	
	protected AbstractDisplay disp; 

	protected State state; 
	
	protected android.content.Context parent; 

	public Calc() {
		A = 0d;
		B = 0d;
		M = 0d;
		op = null;
		changeState(NumberAState.getInstance());
	}
	
	public void setDisp(TextView txt,android.content.Context parent){
		this.disp = new StringDisplay(txt);
		this.parent = parent;
	}

	public void onButtonNumber(Number num) {
		state.onInputNumber(this, num);
	}

	public void onButtonOp(Operation op) {
		state.onInputOperation(this, op);
	}

	public void onButtonClear() {
		state.onInputClear(this);
	}

	public void onButtonAllClear() {
		state.onInputAllClear(this);
	}

	public void onButtonEquale() {
		state.onInputEquale(this);
	}
	public void onMemoryClear(){
		state.onInputMemoryClear(this);
	}
	public void onMemoryLoad(){
		state.onInputMemoryLoad(this);
	}
	public void onMemoryPlus(){
		state.onInputMemoryPlus(this);
	}
	public void onMemoryMinus(){
		state.onInputMemoryMinus(this);
	}
	public void onParsent(){
		state.onInputParsent(this);
	}

	
	public void addDisplayNumber(Number num) {

		if (num == Number.ZERO || num == Number.DOUBLE_ZERO) {
			if (disp.displayChar.size() == 0 && !disp.commaMode) {
				disp.showDisplay(false);
				return;
			}
		}

		if (num == Number.COMMA && !disp.commaMode && disp.displayChar.size() == 0) {
			disp.onInputNumber(Number.ZERO);
		}

		disp.onInputNumber(num);
		disp.showDisplay(false);

	}
	
	
	public void clearDisplay() {
		disp.clear();
		disp.showDisplay(false);
	}

	
	public void clearA() {
		A = 0d;

	}

	
	public void clearB() {
		B = 0d;
	}
	
	public void clearM(){
		M = 0d;
			Toast.makeText(parent, "Memory Clear", Toast.LENGTH_LONG).show();
	}

	
	public double doOperation() throws CalcException{
		double result = op.eval(A, B);

		//エラー検知
		if (Double.isInfinite(result) || Double.isNaN(result)) {
			throw new CalcException();
		}		

		showDisplay(result);
		
		//桁溢れ検知
		if (disp.isOverflow(result)) {
			throw new CalcException();
		}

		return result;
	}
	
	public double doParsentOperation() throws CalcException {
		double result;
		if(getOp() == Operation.TIMES){
			result = A * (B * 0.01);
		}else{
			result = A / B;
			result = result * 100;
		}
		//エラー検知
		if (Double.isInfinite(result) || Double.isNaN(result)) {
			throw new CalcException();
		}		

		showDisplay(result);
		//桁溢れ検知
		if (disp.isOverflow(result)) {
			throw new CalcException();
		}

		return result;
	}

	
	public void saveDisplayNumberToA() {
		A = disp.getNumber();
	}

	
	public void saveDisplayNumberToB() {
		B = disp.getNumber();
	}
	
	public void saveDisplayNumberToM_minus() {
		M -= disp.getNumber();
	}

	public void saveDisplayNumberToM_plus() {
		M += disp.getNumber();
	}

	
	public void showDisplay() {
		disp.showDisplay(false);
	}

	
	public void showDisplay(double d) {
		disp.setNumber(d);
		disp.showDisplay(true);
	}
	
	public Operation getOp() {
		return op;
	}
	
	public void setOp(Operation op) {
		this.op = op;
	}

	public double getA() {
		return A;
	}

	public double getB() {
		return B;
	}
	
	public double getM(){
		return M;
	}

	
	public void changeState(State state) {
		this.state = state;
	}

	
	public void copyAtoB() {
		B = A;
	}

	public void clearError() {
		disp.clearError();
	}
	
	public void setError() {
		if (parent != null){
			Toast.makeText(parent, "Error", Toast.LENGTH_LONG).show();
		}
		disp.setError();
	}


	public void changeSign() {
		if (disp.getNumber() != 0d) {
			disp.minus = !disp.minus;
			disp.showDisplay(false);
		}
	}

	public void clearMH() {
		MH = true;
	}

	public boolean getMH() {
		return MH;
	}

	public void setMH(boolean boo) {
		MH = boo;
		
	}
}
