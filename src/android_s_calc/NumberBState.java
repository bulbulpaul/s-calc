package android_s_calc;

public class NumberBState implements State {

	private static NumberBState singleton = new NumberBState();

	private NumberBState() { 
	}

	public static State getInstance() { 
		return singleton;
	}

	//数値ボタン処理
	public void onInputNumber(Context context, Number num) {
		if(context.getMH() == false){
			context.clearDisplay();
		}
		context.addDisplayNumber(num);
		context.clearMH();
	}

	//演算子ボタン処理
	public void onInputOperation(Context context, Operation op) {
		try {
			context.saveDisplayNumberToB();
			context.doOperation();
			context.setOp(op);
			context.saveDisplayNumberToA();
			context.changeState(OperationState.getInstance());			
		} catch (CalcException e) {
			context.setError();
			context.clearA();
			context.clearB();
			context.clearDisplay();
			context.clearError();
			context.changeState(NumberAState.getInstance());
		}
	}

	//イコールボタン処理
	public void onInputEquale(Context context) {
		try {
			context.saveDisplayNumberToB();
			context.doOperation();
			context.changeState(ResultState.getInstance());
		} catch (CalcException e) {
			context.setError();
			context.clearA();
			context.clearB();
			context.clearDisplay();
			context.clearError();
			context.changeState(NumberAState.getInstance());
		}
	}

	//クリアボタン処理
	public void onInputClear(Context context) {
		context.clearB();
		context.clearDisplay();
	}

	//オールクリア処理
	public void onInputAllClear(Context context) {
		context.clearA();
		context.clearB();
		context.clearDisplay();
		context.changeState(NumberAState.getInstance());
	}
	
	//メモリクリア処理
	public void onInputMemoryClear(Context context) {
		context.clearM();
	}
	
	//メモリロード処理
	public void onInputMemoryLoad(Context context) {
		context.clearDisplay();
		context.showDisplay(context.getM());
		context.setMH(false);
	}
	
	//メモリプラス処理
	public void onInputMemoryPlus(Context context) {
		try {
			context.saveDisplayNumberToB();
			context.doOperation();
			context.saveDisplayNumberToM_plus();
			context.changeState(ResultState.getInstance());
		} catch (CalcException e) {
			context.setError();
			context.clearA();
			context.clearB();
			context.clearDisplay();
			context.clearError();
			context.changeState(NumberAState.getInstance());
		}
	}

	//メモリマイナス処理
	public void onInputMemoryMinus(Context context){
		try {
			context.saveDisplayNumberToB();
			context.doOperation();
			context.saveDisplayNumberToM_minus();
			context.changeState(ResultState.getInstance());
		} catch (CalcException e) {
			context.setError();
			context.clearA();
			context.clearB();
			context.clearDisplay();
			context.clearError();
			context.changeState(NumberAState.getInstance());
		}
	}

	//パーセントボタン処理
	public void onInputParsent(Context context) {
		switch (context.getOp()) {
		case DIVIDE:
			try {
				context.saveDisplayNumberToB();
				context.doParsentOperation();
				context.changeState(ResultState.getInstance());
			} catch (CalcException e) {
				context.setError();
				context.clearA();
				context.clearB();
				context.clearDisplay();
				context.clearError();
				context.changeState(NumberAState.getInstance());

			}
			break;
		case TIMES:
			try {
				context.saveDisplayNumberToB();
				context.doParsentOperation();
				context.changeState(ResultState.getInstance());
			} catch (CalcException e) {
				context.setError();
				context.clearA();
				context.clearB();
				context.clearDisplay();
				context.clearError();
				context.changeState(NumberAState.getInstance());

			}
			break;
		case MINUS:
			context.clearA();
			context.clearB();
			context.clearDisplay();
			context.changeState(NumberAState.getInstance());
			break;
		case PLUS:
			context.clearA();
			context.clearB();
			context.clearDisplay();
			context.changeState(NumberAState.getInstance());
			break;
		default:
			break;
		}
	}
}
