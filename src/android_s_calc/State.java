package android_s_calc;

public interface State {
	
	public abstract void onInputNumber(Context context,Number num);
	
	public abstract void onInputOperation(Context context, Operation op);

	public abstract void onInputEquale(Context context);

	public abstract void onInputClear(Context context);

	public abstract void onInputAllClear(Context context);
	
	public abstract void onInputMemoryClear(Context context);
	
	public abstract void onInputMemoryLoad(Context context);
	
	public abstract void onInputMemoryPlus(Context context);
	
	public abstract void onInputMemoryMinus(Context context);
	
	public abstract void onInputParsent(Context context);
}
