package android_s_calc;

public class ResultState implements State {
	private static ResultState singleton = new ResultState();

	private ResultState() { 
	}

	public static State getInstance() { 
		return singleton;
	}

	//数値ボタン処理
	public void onInputNumber(Context context, Number num) {
		context.clearDisplay();
		context.addDisplayNumber(num);
		context.changeState(NumberAState.getInstance());
	}

	//演算子ボタン処理
	public void onInputOperation(Context context, Operation op) {
		context.saveDisplayNumberToA();
		context.setOp(op);
		context.changeState(OperationState.getInstance());
	}

	//イコールボタン処理
	public void onInputEquale(Context context) {
	}

	//クリアボタン処理
	public void onInputClear(Context context) {
		context.clearA();
		context.clearB();
		context.clearDisplay();
		context.changeState(NumberAState.getInstance());
	}

	//オールクリア処理
	public void onInputAllClear(Context context) {
		context.clearA();
		context.clearB();
		context.clearDisplay();
		context.changeState(NumberAState.getInstance());
	}

	//メモリクリア処理
	public void onInputMemoryClear(Context context) {
		context.clearM();
	}

	//メモリロード処理
	public void onInputMemoryLoad(Context context) {
		context.clearDisplay();
		context.showDisplay(context.getM());
		context.setMH(false);
		context.changeState(NumberAState.getInstance());
	}

	//メモリプラス処理
	public void onInputMemoryPlus(Context context) {
		context.saveDisplayNumberToM_plus();
	}

	//メモリマイナス処理
	public void onInputMemoryMinus(Context context){
		context.saveDisplayNumberToM_minus();
	}

	//パーセントボタン処理
	public void onInputParsent(Context context) {
		context.clearA();
		context.clearB();
		context.clearDisplay();
		context.changeState(NumberAState.getInstance());
	}
}
