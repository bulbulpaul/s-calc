package android_s_calc;

public enum Operation {
	PLUS {
		double eval(double x, double y) {
			return x + y;
		}
	},
	MINUS {
		double eval(double x, double y) {
			return x - y;
		}
	},
	TIMES {
		double eval(double x, double y) {
			return x * y;
		}
	},
	DIVIDE {
		double eval(double x, double y) {
			return x / y;
		}
	},
	PARSENT {
		double eval(double x, double y){
			return x * (y * 0.01);
		}
	};
	

	abstract double eval(double x, double y);
}
